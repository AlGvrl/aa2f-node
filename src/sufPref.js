var path = require('path'),
    gen = require(path.join(__dirname, 'generator.js'));

exports.extend = extend;
exports.extendRight = extendRight;
exports.extendLeft = extendLeft;

function extend(word, cardinality, toTheRight){
    if (toTheRight) {
        return gen.generateAA2F(word, cardinality);
    } else {
        return extendLeft(word, cardinality);
    }
}

function extendLeft(word, cardinality){
    var result = [],
        str,
        aCode = 'a'.charCodeAt(0);

    for (var i = 0; i < cardinality; i++) {
        str = String.fromCharCode(i + aCode) + word;//just 2 lines changed
        if (gen.isAA2FFullCheck(str, cardinality)) {//compared to generateAA2F
            result.push(str);
        }
    }
    return result;
}

function extendRight(word, cardinality){
    return gen.generateAA2F(word, cardinality);
}