/*This is more of a draft just to get whole thing working. Will be, hopefuly, rewritten*/
(module.exports = function(){
    'use strict';
    var cp = require('child_process'),
        path = require('path'),
        stringStack = [''],
        cardinality = 3,
        numCPUs = require('os').cpus().length,
        childProcesses = [],
        maxWordLength = 0,
        mode = 'AA2F',
        argv = require('minimist')(process.argv.slice(2)),
        fs = require('fs');

    function httpHandler(req, res) {
        var filename = path.join(__dirname, 'index.html'),
            readstream = fs.createReadStream(filename);

        readstream.on('open', function() {
            readstream.pipe(res);
        });

        readstream.on('error', function(err) {
            res.end(err);
        });
    }

    if (argv.l || argv.load) {
        var inputPath = argv.l || argv.load,
            inputData,
            inputJson;
        inputPath = path.isAbsolute(inputPath) ? inputPath : path.join(__dirname,'..', inputPath);
        try {
            inputData = fs.readFileSync(inputPath, 'utf8');
        } catch (e) {
            console.log("Input file doesn't exist");
            console.log(e);
            process.exit(1);
        }

        try {
            inputJson = JSON.parse(inputData);
        } catch (e) {
            console.log("Input file contents can't be parsed as JSON");
            process.exit(1);
        }
        stringStack = inputJson;
    }

    if (argv.w || argv.webInterface) {
        var httpServer = require('http').createServer(httpHandler),
        io = require('socket.io')(httpServer);

        httpServer.listen(80);

        io.on('connection', function(socket) {
            socket.on('updateRequest', function(data) {
                socket.emit('update', stringStack);
            });
        });
    }

    if (argv.r) {
        mode = 'AA2FR';
    }

    function msgHandler(child, msg) {
        while (msg.length > 0) {
            var lastWord = msg.pop();
            if (lastWord.length > maxWordLength) {
                maxWordLength = lastWord.length;
                console.log(lastWord.length + ' ' + lastWord);
            }
            stringStack.push(lastWord);
        }
        var strsToSend = [];
        if (stringStack.length > 0) {
            strsToSend.push(stringStack.pop());
        }
        child.send({
            parentStrings: strsToSend,
            cardinality: cardinality,
            mode: mode
        });
    }

    for (var i = 0; i < numCPUs - 1; i++) {
        var newChild = cp.fork(path.join(__dirname, 'workerProcess.js'));
        newChild.on('message', msgHandler.bind(null, newChild));
        childProcesses.push(newChild);
    }
}());
