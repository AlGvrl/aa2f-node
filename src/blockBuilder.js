var path = require('path'),
    gen = require(path.join(__dirname, '../src/generator.js')),
    A2FWordListMemoized = memoize(A2FWordList);

exports.stringToBlocks = stringToBlocks;
exports.blocksToString = blocksToString;
exports.combineBlocks = combineBlocks;
exports.wordFromBlocks = wordFromBlocks;

function memoize(func) {
    var cachedResults = Object.create(null);
    return function() {
        var argsStr = Array.prototype.join.call(arguments, ';');
        if (!cachedResults[argsStr]) {
            cachedResults[argsStr] = func.apply(null, arguments);
        }
        return cachedResults[argsStr];
    };
}

function A2FWordList(maxLength, cardinality, currentWord) {
    var result = [];
    if (currentWord == null) {
        currentWord = '';
    } else {
        result = gen.isA2F(currentWord) ? [currentWord] : [];
    }
    if (currentWord.length < maxLength) {
        var aCode = 'a'.charCodeAt(0);
        for (var i = 0; i < cardinality; i++) {
            var nextWord = currentWord + String.fromCharCode(i + aCode),
                recursionResult = A2FWordList(maxLength, cardinality, nextWord);
            result = result.concat(recursionResult);
        }
    }
    return result;
}

function stringToBlocks(allBlocks, str) {
    var result = [],
        strArray = str.toLowerCase().split('');
    while (strArray.length) {
        var type, letter, block, shiftTwo = false;
        if (strArray[0] === 's') {
            type = 'suffix';
            letter = strArray[1];
            shiftTwo = true;
        } else if (strArray.length > 1 && strArray[1] === 'p') {
            type = 'prefix';
            letter = strArray[0];
            shiftTwo = true;
        } else {
            type = 'factor';
            letter = strArray[0];
        }
        strArray.shift();
        if (shiftTwo) {
            strArray.shift();
        }
        block = allBlocks.filter(function(v) {
            return v.letter.toLowerCase() === letter && v.type.toLowerCase() === type;
        })[0];
        result.push(block);
    }
    return result;
}

function blocksToString(blocksToConvert) {
    var result = '';
    for (var i = 0; i < blocksToConvert.length; i++) {
        if (blocksToConvert[i].type === 'suffix') {
            result += 's' + blocksToConvert[i].letter;
        } else if (blocksToConvert[i].type === 'prefix') {
            result += blocksToConvert[i].letter + 'p';
        } else {
            result += blocksToConvert[i].letter;
        }
    }
    return result;
}

function wordFromBlocks(blocksToConvert) {
    var result = '';
    for (var i = 0; i < blocksToConvert.length; i++) {
        result += blocksToConvert[i].value;
    }
    return result;
}

function combineBlocks(blocks, current) {
    if (current && current[current.length - 1].type === 'prefix') {
        return [current];
    }
    current = current || [];
    var result = [],
        newCombination,
        recursionResult,
        i,
        suffixes = blocks.filter(function(val) {return val.type === 'suffix';}),
        prefixes = blocks.filter(function(val) {return val.type === 'prefix';}),
        factors = blocks.filter(function(val) {return val.type === 'factor';}),
        factorCombos = A2FWordListMemoized(10, factors.length);

    if (current.length === 0) {
        newCombination = [];
        for (i = 0; i < suffixes.length; i++) {
            newCombination[0] = suffixes[i];
            recursionResult = combineBlocks(blocks, newCombination.slice());
            result = result.concat(recursionResult);
        }
    } else {
        if (current && current[current.length - 1].type === 'suffix' && factorCombos.length > 0) {
            for (i = 0; i < factorCombos.length; i++) {
                var blocksFromCombo = stringToBlocks(blocks, factorCombos[i]);
                if (blocksFromCombo[0].letter === current[current.length - 1].letter) {
                    continue;
                }
                newCombination = current.concat(blocksFromCombo);
                recursionResult = combineBlocks(blocks, newCombination.slice());
                result = result.concat(recursionResult);
            }
        }

        newCombination = current;
        for (i = 0; i < prefixes.length; i++) {
            if (prefixes[i].letter === current[current.length - 1].letter) {
                continue;
            }
            newCombination.push(prefixes[i]);
            recursionResult = combineBlocks(blocks, newCombination.slice());
            result = result.concat(recursionResult);
            newCombination.pop();
        }
    }
    return result;
}