'use strict';
var path = require('path'),
    gen = require(path.join(__dirname, 'generator.js')),
    _ = require('lodash');

function msgHandler(msg) {
    var result = [],
        genFunction = msg.mode === 'AA2FR' ? gen.generateAA2FR : gen.generateAA2F;
    for (var i = 0; i < msg.parentStrings.length; i++) {
        result.push(genFunction(msg.parentStrings[i], msg.cardinality));
    }
    result = _.flatten(result);
    process.send(result);
}

process.on('message', msgHandler);
process.send([]);
