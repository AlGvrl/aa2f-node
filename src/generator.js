/** @module generator */
"use strict";
var _ = require('lodash');

/**
 * Returns array of up to *cardinality* direct descendants of *word* that
 * are almost abelian square free
 * @param {String} word        Prefix
 * @param {Number} cardinality Cardinality of alphabet (0-th symbol is 'a')
 * @returns {Array.<String>} Direct descendants of word
 * @memberof module:generator
 */
function generateAA2F(word, cardinality) {
    var result = [],
        str,
        aCode = 'a'.charCodeAt(0);

    for (var i = 0; i < cardinality; i++) {
        str = word + String.fromCharCode(i + aCode);
        if (isAA2FPartialCheck(str, cardinality)) {
            result.push(str);
        }
    }
    return result;
}

function endsWithDouble(wrd) {
        var lastIdx = wrd.length - 1;
        if (wrd.length < 2) {
            return false;
        }
        return (wrd[lastIdx] === wrd[lastIdx - 1]);
    }

function endsWithTriple(wrd) {
    var lastIdx = wrd.length - 1;
    if (wrd.length < 3) {
        return false;
    }
    return ((wrd[lastIdx] === wrd[lastIdx - 1]) &&
            (wrd[lastIdx - 1] === wrd[lastIdx - 2]));
}

/**
 * Returns array of up to *cardinality* descendants of *word* that are almost
 * abelian square free with restrictions (i.e. have no doubles, only triples)
 * @param {String} word        Prefix
 * @param {Number} cardinality Cardinality of alphabet (0-th symbol is 'a')
 * @returns {Array.<String>} Descendants of word
 * @memberof module:generator
 */
function generateAA2FR(word, cardinality) {
    var result = [],
        str,
        aCode = 'a'.charCodeAt(0);

    if (endsWithDouble(word) && !endsWithTriple(word)) {
        if (isAA2FPartialCheck(word + word[word.length - 1], cardinality)) {
            return [word + word[word.length - 1]];
        } else {
            return [];
        }
    }

    for (var i = 0; i < cardinality; i++) {
        str = word + String.fromCharCode(i + aCode);
        if (endsWithDouble(str)) {
            if (isAA2FPartialCheck(str, cardinality)) {
                str += str[str.length - 1];
                if (isAA2FPartialCheck(str, cardinality)) {
                    result.push(str);
                }
            }
        } else {
            if (isAA2FPartialCheck(str, cardinality)) {
                result.push(str);
            }
        }
    }
    return result;
}

/**
 * Returns true if word is almost abelian square free, false othewise
 * @param {String} word        Word to be checked
 * @param {Number} cardinality Cardinality of alphabet
 * @returns {Boolean} Result of the test
 * @memberof module:generator
 */
function isAA2FFullCheck(word, cardinality) {
    var len = word.length,
        parikhCalc = calculateParikhVector(word, cardinality),
        vec1,
        vec2;

    for (var i = 0; i < len - 3; i++) {
        for (var j = 2; j <= Math.floor((len-i)/2); j++) {
            vec1 = parikhCalc(i, j);
            vec2 = parikhCalc(i + j, j);
            if (_.isEqual(vec1, vec2)) {
                return false;
            }
        }
    }
    return true;
}

function isA2F(word, cardinality) {
    var len = word.length,
        parikhCalc = calculateParikhVector(word, cardinality),
        vec1,
        vec2;

    for (var i = 0; i < len - 1; i++) {
        for (var j = 1; j <= Math.floor((len-i)/2); j++) {
            vec1 = parikhCalc(i, j);
            vec2 = parikhCalc(i + j, j);
            if (_.isEqual(vec1, vec2)) {
                return false;
            }
        }
    }
    return true;
}

/**
 * Similar to {@link module:generator.isAA2FFullCheck isAA2FFullCheck}
 * but only checks factors that include last letter.
 * @param {String}  word        Word to be checked
 * @param {Number}  cardinality Cardinality of alphabet
 * @returns {Boolean} Result of the test
 * @memberof module:generator
 */
function isAA2FPartialCheck(word, cardinality) {
    var len = word.length,
        parikhCalc = calculateParikhVector(word, cardinality),
        vec1,
        vec2,
        factorLen = 0;

    for (var i = len - 4; i >= 0; i -= 2) {
        factorLen = (len - i) / 2;
        vec1 = parikhCalc(i, factorLen);
        vec2 = parikhCalc(i + factorLen, factorLen);
        if (_.isEqual(vec1, vec2)) {
            return false;
        }
    }
    return true;
}

function zeroFilledArray(size) {
    var res = [];
    for (var i = 0; i < size; i++) {
        res[i] = 0;
    }
    return res;
}

function getCardinality(w) {
    if (!w) {
        return 0;
    }
    var sortedUnique = _.uniq(w).sort();
    return sortedUnique[sortedUnique.length-1].charCodeAt(0) -
           sortedUnique[0].charCodeAt(0) + 1;
}

/**
 * @function calculatorFunction
 * @desc Use this function to get the Parikh vector.
 * @param {Number} [firstIndex=0] Index of the first symbol of a factor in the word.
 * @param {Number} [length]     How many symbols factor has. Defaults to
 * "until the end".
 * @returns {Array.<Number>} Parikh vector of factor.
 * @memberof module:generator
*/

/**
 * Creates a closure function which allows to calculate parikh vector of any
 * factor of the word.
 * @param {String}             word         Word itself
 * @param {Number}             cardinality  Cardinality of alphabet
 * @param {Boolean}            sanitizeWord Set this to true if word might
 *                                          contain unwanted characters
 * @returns {calculatorFunction} Function to be used for the actual vector
 * calculation.
 * @see {@link module:generator.calculatorFunction calculatorFunction}
 * @memberof module:generator
 */
function calculateParikhVector(word, cardinality, sanitizeWord) {
    if (sanitizeWord) {
        word = Array.prototype.filter.call(word, function (value) {
            return value >= 'a' && value <= 'z';
        }).join('');
    }
    cardinality = cardinality || getCardinality(word);
    var cumulativeVector = Array.prototype.reduce.call(word, function (accum, val) {
        var newElem = accum[accum.length - 1].slice();
        newElem[val.charCodeAt(0) - 'a'.charCodeAt(0)] += 1;
        accum.push(newElem);
        return accum;
    }, [zeroFilledArray(cardinality)]);

    return function (firstIndex, length) {
        if (firstIndex == null) {
            firstIndex = 0;
        }
        length = length || word.length - firstIndex;

        var startVector = cumulativeVector[firstIndex];
        var endVector = cumulativeVector[firstIndex + length];
        var result = zeroFilledArray(cardinality);

        for (var i = 0; i < cardinality; i++) {
            result[i] = endVector[i] - startVector[i];
        }
        return result;
    };
}

exports.generateAA2F = generateAA2F;
exports.generateAA2FR = generateAA2FR;
exports.isAA2FPartialCheck = isAA2FPartialCheck;
exports.isA2F = isA2F;
exports.isAA2FFullCheck = isAA2FFullCheck;
exports.calculateParikhVector = calculateParikhVector;
