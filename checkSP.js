"use strict"
var path = require('path'),
    fs = require('fs'),
    minimist = require('minimist'),
    sp = require(path.join(__dirname, 'src/sufPref.js')),
    blockB = require(path.join(__dirname, 'src/blockBuilder.js')),
    gen = require(path.join(__dirname, 'src/generator.js')),
    argv = minimist(process.argv.slice(2)),
    inputPath,
    inputData,
    inputJson,
    successPath,
    successStream,
    failurePath,
    failureStream;

inputPath = argv.i || argv.input;
successPath = argv.s || argv.success;
failurePath = argv.f || argv.failure;
inputPath = path.isAbsolute(inputPath) ? inputPath : path.join(__dirname, inputPath);
successPath = path.isAbsolute(successPath) ? successPath : path.join(__dirname, successPath);
failurePath = path.isAbsolute(failurePath) ? failurePath : path.join(__dirname, failurePath);

try {
    inputData = fs.readFileSync(inputPath, 'utf8');
} catch (e) {
    console.log("Input file doesn't exist");
    console.log(e);
    process.exit(1);
}

try {
    inputJson = JSON.parse(inputData);
} catch (e) {
    console.log("Input file contents can't be parsed as JSON");
    process.exit(1);
}

successStream = fs.createWriteStream(successPath);
failureStream = fs.createWriteStream(failurePath);

inputJson.forEach(function(testCase) {
    var blocks = testCase.blocks,
        timeLimit = testCase.timeLimit * 1000 || 60000,
        cardinality = testCase.cardinality || 3,
        combinations = blockB.combineBlocks(blocks),
        wordQueue = [],
        word = '',
        result = {},
        failed = false,
        blockString = '',
        currentItem,
        genResult,
        startTime;

    combinations.forEach(function(combination) {
        result[blockB.blocksToString(combination)] = 'Not processed';
    });

    for (var i = 0; i < combinations.length; i++) {
        blockString = blockB.blocksToString(combinations[i]);
        word = blockB.wordFromBlocks(combinations[i]);
        wordQueue = [];
        startTime = Date.now();
        if (!gen.isAA2FFullCheck(word)) {
            failed = true;
            result[blockString] = "Combination isn't AA2F";
            break;
        }

        wordQueue.push({
            word: word,
            charsToRight: last(combinations[i]).lengthLimit,
            charsToLeft: first(combinations[i]).lengthLimit,
            extendToRight: true
        });

        while (wordQueue.length) {
            if ((Date.now() - startTime) > timeLimit) {
                break;
            }
            currentItem = wordQueue.shift();
            if (currentItem.charsToRight === 0 && currentItem.charsToLeft === 0) {
                result[blockString] = currentItem.word;
                break;
            }
            if (currentItem.extendToRight && currentItem.charsToRight) {
                genResult = sp.extend(currentItem.word, cardinality, true);
                genResult.forEach(function(x) {
                    wordQueue.push({
                        word: x,
                        charsToRight: currentItem.charsToRight - 1,
                        charsToLeft: currentItem.charsToLeft,
                        extendToRight: false
                    });
                });
            } else if (!currentItem.extendToRight && currentItem.charsToLeft) {
                genResult = sp.extend(currentItem.word, cardinality, false);
                genResult.forEach(function(x) {
                    wordQueue.push({
                        word: x,
                        charsToRight: currentItem.charsToRight,
                        charsToLeft: currentItem.charsToLeft - 1,
                        extendToRight: true
                    });
                });
            }
        }

        if(result[blockString] === 'Not processed') {
            if (Date.now() - startTime > timeLimit) {
                result[blockString] = 'Out of time';
                failed = true;
                break;
            } else {
                result[blockString] = "Couldn't reach required length";
                failed = true;
                break;
            }
        }
    }

    if (failed) {
        failureStream.write(JSON.stringify(result, null,'\t') + '\n', 'utf8');
    } else {
        successStream.write(JSON.stringify(result, null,'\t') + '\n', 'utf8');
    }
});

function first(arr) {
    return arr.length ? arr[0] : null;
}
function last(arr) {
    return arr.length ? arr[arr.length - 1] : null;
}