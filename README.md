# AA2F Generator
##Introduction
I decided to try to write (as a test of concept if parallelizing is viable in this case) a multithreaded (or multi-processed(?) in this case) version of the [generator](../../../aa2f-generator) and went
with the language I am most comfortable with -- Javascript.

See also the similar program written in [Rust](../../../aa2f-rs).

## Installation

(This may be a little bit too in depth)

1. Install [git](http://git-scm.com/).
2. Install [Node.js](https://nodejs.org/) (and **n**ode **p**ackage **m**anager).
3. [Optional] Check if they are working by running following in command line:

        git --version
        node -v
        npm -v

4. `cd` into the directory you wish to clone project to.
5. Clone the repository:

        git clone https://AlGvrl@bitbucket.org/AlGvrl/aa2f-node.git

6. `cd` into the project's directory and install dependencies:

        cd aa2f-node
        npm install

7. [Optional] Do `npm run test` to, well, test the code. Everything should be green.
If something fails, contact me. If everything fails, try to install testing 
framework globally (I don't think this will happen, though):

        npm install -g mocha

8. Run the generator itself:

        node index.js
This may (and most probably will) change in future. I will keep this readme updated.
For now everything is outputed into console. Remember that you can redirect it to file like this:

        node index.js > output.txt

## Usage
```
node index.js [-w | --webInterface] [-r] [-l <inputFile> | --load <inputFile>]
```

---

```
-w
--webInterface
```
Launches a simple web interface describing the current status of generator on port 80 of localhost.

---

```
-r
```
Use AA2FR mode instead of AA2F

---

```
-l <inputFile>
--load <inputFile>
```
Set the initial state of generator to the contents of specified file. File should contain [JSON](http://www.json.org/)ish array of strings.

#Suffix/Prefix checking

##Usage

```
node checkSP.js -i <inputFile> -f <failureFile> -s <successFile>
```

---

```
-i <inputFile>
```
File containing information about cases that should be checked/tested. More information about file structure below.

---

```
-f <failureFile>
```
File in which test cases that didn't pass the test will be written.

---

```
-s <successFile>
```
File in which test cases that passed the test will be written.

##Input

Input file is expected to be a JSON file in this format (note that comments are actually not allowed in JSON. Also, some values might be not making sense -- they are just here as a somewhat simple example):

```
[
    {//test case 1
        "timeLimit: 60, //seconds. Defaults to 60 if not specified. Limit for computing extensions of each combination of blocks (combinations: sCADp, sDBABCp etc.)
        "cardinality: 3", //Cardinality of alphabet. Defaults to 3.
        "blocks": [
            {
                "letter": "A",
                "type": "factor",
                "value": "aba"
            },
            {
                "letter": "B",
                "type": "factor",
                "value": "bab"
            },
            {
                "letter": "C",
                "type": "suffix",
                "value": "bc",
                "lengthLimit": 20 //by how many symbols suffix should be extended to the right
            },
            {
                "letter": "C",
                "type": "prefix",
                "value": "cb",
                "lengthLimit": 20 //by how many symbols prefix should be extended to the left
            },
            {
                "letter": "D",
                "type": "suffix",
                "value": "ad",
                "lengthLimit": 20
            },
            {
                "letter": "D",
                "type": "prefix",
                "value": "da",
                "lengthLimit": 20
            }
        ]
    },

    {//test case 2
        ...
    }
]
```

##Output

#####Example of contents of success.txt

```
{//object representing results of first successful test case
    "sABp" : "string-That-Is-Long-Enough-To-Reach-Needed-Length",
    "sACp" : "...",
    ...
    "sDCp" : "..."
}

{//object representing results of seconds successful test case
    ...
}
```

Note that while result of each individual test case is valid JSON, the contents of the file as a whole are not. However, only `[` at the beginning of file, `]` at the end and `,` between test cases are missing.

#####Example of contents of failure.txt

```
{
    "sABp" : "string-That-Is-Long-Enough-To-Reach-Needed-Length",
    "sACp" : "string-That-Is-Long-Enough-To-Reach-Needed-Length",
    "sADp" : "Could not reach required length",
    "sBAp" : "Not processed",
    ...
    "sDCp" : "Not processed"
}

{
    "sABp" : "string-That-Is-Long-Enough-To-Reach-Needed-Length",
    "sACp" : "string-That-Is-Long-Enough-To-Reach-Needed-Length",
    "sADp" : "Out of time",
    "sBAp" : "Not processed",
    ...
    "sDCp" : "Not processed"
}

{
    "sABp" : "string-That-Is-Long-Enough-To-Reach-Needed-Length",
    "sACp" : "string-That-Is-Long-Enough-To-Reach-Needed-Length",
    "sADp" : "Combination isn't AA2F",
    "sBAp" : "Not processed",
    ...
    "sDCp" : "Not processed"
}

...
```