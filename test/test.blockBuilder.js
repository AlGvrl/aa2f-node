"use strict";
var chai = require('chai'),
    should = chai.should(),
    mocha = require('mocha'),
    blockB = require('../src/blockBuilder.js');

describe('blockBuilder', function() {
    var blocksFor4Pairs = [
        {letter: 'A', type: 'prefix'},
        {letter: 'A', type: 'suffix'},
        {letter: 'B', type: 'prefix'},
        {letter: 'B', type: 'suffix'},
        {letter: 'C', type: 'prefix'},
        {letter: 'C', type: 'suffix'},
        {letter: 'D', type: 'prefix'},
        {letter: 'D', type: 'suffix'}
    ],
    blocksForFixedA = [
        {letter: 'A', type: 'factor'},
        {letter: 'B', type: 'prefix'},
        {letter: 'B', type: 'suffix'},
        {letter: 'C', type: 'prefix'},
        {letter: 'C', type: 'suffix'},
        {letter: 'D', type: 'prefix'},
        {letter: 'D', type: 'suffix'}
    ],
    blocksForFixedAB = [
        {letter: 'A', type: 'factor'},
        {letter: 'B', type: 'factor'},
        {letter: 'C', type: 'prefix'},
        {letter: 'C', type: 'suffix'},
        {letter: 'D', type: 'prefix'},
        {letter: 'D', type: 'suffix'}
    ],
    blocksForFixedABC = [
        {letter: 'A', type: 'factor'},
        {letter: 'B', type: 'factor'},
        {letter: 'C', type: 'factor'},
        {letter: 'D', type: 'prefix'},
        {letter: 'D', type: 'suffix'}
    ];

    describe('wordFromBlocks', function() {
        it('test case 1', function() {
            var input = [
                {letter: 'C', type: 'suffix', value: 'abca'},
                {letter: 'B', type: 'factor', value: 'bab'},
                {letter: 'A', type: 'factor', value: 'aca'},
                {letter: 'B', type: 'factor', value: 'bab'},
                {letter: 'D', type: 'prefix', value: 'cbba'}
            ];

            blockB.wordFromBlocks(input).should.equal('abcababacababcbba');
        });

        it('test case 2', function() {
            var input = [
                {letter: 'A', type: 'factor', value: 'a'},
                {letter: 'B', type: 'factor', value: ''},
                {letter: 'C', type: 'factor', value: 'c'}
            ];

            blockB.wordFromBlocks(input).should.equal('ac');
        });

        it('test case 3', function() {
            var input = [
                {letter: 'C', type: 'suffix', value: 'cbc'},
                {letter: 'B', type: 'prefix', value: 'abc'}
            ];

            blockB.wordFromBlocks(input).should.equal('cbcabc');
        });
    });

    describe('stringToBlocks', function() {
        it('should parse "sCBADp"', function() {
            var expected = [
                {letter: 'C', type: 'suffix'},
                {letter: 'B', type: 'factor'},
                {letter: 'A', type: 'factor'},
                {letter: 'D', type: 'prefix'}
            ];

            blockB.stringToBlocks(blocksForFixedAB, 'sCBADp').should.deep.equal(expected);
        });

        it('should parse "ABC"', function() {
            var expected = [
                {letter: 'A', type: 'factor'},
                {letter: 'B', type: 'factor'},
                {letter: 'C', type: 'factor'}
            ];

            blockB.stringToBlocks(blocksForFixedABC, 'ABC').should.deep.equal(expected);
        });

        it('should parse "sCBp"', function() {
            var expected = [
                {letter: 'C', type: 'suffix'},
                {letter: 'B', type: 'prefix'}
            ];

            blockB.stringToBlocks(blocksForFixedA, 'sCBp').should.deep.equal(expected);
        });
    });

    describe('blocksToString', function() {
        it('should produce "sCBADp"', function() {
            var input = [
                {letter: 'C', type: 'suffix'},
                {letter: 'B', type: 'factor'},
                {letter: 'A', type: 'factor'},
                {letter: 'D', type: 'prefix'}
            ];

            blockB.blocksToString(input).should.deep.equal('sCBADp');
        });

        it('should produce "ABC"', function() {
            var input = [
                {letter: 'A', type: 'factor'},
                {letter: 'B', type: 'factor'},
                {letter: 'C', type: 'factor'}
            ];

            blockB.blocksToString(input).should.deep.equal('ABC');
        });

        it('should produce "sCBp"', function() {
            var input = [
                {letter: 'C', type: 'suffix'},
                {letter: 'B', type: 'prefix'}
            ];

            blockB.blocksToString(input).should.deep.equal('sCBp');
        });
    });

    describe('combineBlocks', function() {
        it('should produce all combinations when nothing is fixed', function() {
            var expected = ['sABp', 'sACp', 'sADp', 'sBAp', 'sBCp', 'sBDp',
                           'sCAp', 'sCBp', 'sCDp', 'sDAp', 'sDBp', 'sDCp'].sort();
            blockB.combineBlocks(blocksFor4Pairs).map(function(v) {
                return blockB.blocksToString(v);
            }).sort().should.deep.equal(expected);
        });

        it('should produce all combinations when A is fixed', function() {
            var expectedStr = 'sBABp, sBACp, sBADp, sCABp, sCACp, sCADp,sDABp, sDACp, sDADp, sBCp, sBDp, sCBp, sCDp, sDBp, sDCp'.replace(/[^ABCDsp,]/gim, ''),
                expected = expectedStr.split(',').sort();
            blockB.combineBlocks(blocksForFixedA).map(function(v) {
                return blockB.blocksToString(v);
            }).sort().should.deep.equal(expected);
        });

        it('should produce all combinations when A and B are fixed', function() {
            var expectedStr = 'sCACp, sCADp, sCBCp, sCBDp, sCABCp, sCABDp, sCBACp, sCBADp, sCABACp,sCABADp, sCBABCp, sCBABDp, sDACp, sDADp, sDBCp, sDBDp, sDABCp, sDABDp, sDBACp, sDBADp, sDABACp, sDABADp, sDBABCp, sDBABDp, sCDp, sDCp'.replace(/[^ABCDsp,]/gim, ''),
                expected = expectedStr.split(',').sort();
            blockB.combineBlocks(blocksForFixedAB).map(function(v) {
                return blockB.blocksToString(v);
            }).sort().should.deep.equal(expected);
        });

        it('should produce all combinations when A, B and C are fixed', function() {
            var expectedStr = 'sDADp, sDBDp, sDCDp, sDABDp, sDACDp, sDBADp, sDBCDp, sDCADp, sDCBDp, sDABADp,sDABCDp, sDACADp, sDACBDp, sDBABDp, sDBACDp, sDBCADp, sDBCBDp, sDCABDp,sDCACDp, sDCBADp, sDCBCDp, sDABACDp, sDABCADp, sDABCBDp, sDACABDp, sDACBADp,sDACBCDp, sDBABCDp, sDBACADp, sDBACBDp, sDBCABDp, sDBCACDp, sDBCBADp, sDCABADp,sDCABCDp, sDCACBDp, sDCBABDp, sDCBACDp, sDCBCADp, sDABACADp, sDABACBDp,sDABCABDp, sDABCACDp, sDABCBADp, sDACABADp, sDACABCDp, sDACBABDp, sDACBACDp,sDACBCADp, sDBABCADp, sDBABCBDp, sDBACABDp, sDBACBADp, sDBACBCDp, sDBCABADp,sDBCABCDp, sDBCACBDp, sDBCBABDp, sDBCBACDp, sDCABACDp, sDCABCADp, sDCABCBDp,sDCACBADp, sDCACBCDp, sDCBABCDp, sDCBACADp, sDCBACBDp, sDCBCABDp, sDCBCACDp,sDABACABDp, sDABACBADp, sDABACBCDp, sDABCABADp, sDABCBABDp, sDACABACDp,sDACABCADp, sDACABCBDp, sDACBACADp, sDACBCACDp, sDBABCABDp, sDBABCACDp,sDBABCBADp, sDBACABADp, sDBACBABDp, sDBCABCBDp, sDBCACBCDp, sDBCBABCDp,sDBCBACADp, sDBCBACBDp, sDCABACADp, sDCABCACDp, sDCACBABDp, sDCACBACDp,sDCACBCADp, sDCBABCBDp, sDCBACBCDp, sDCBCABADp, sDCBCABCDp, sDCBCACBDp,sDABACABADp, sDABACBABDp, sDABCBABCDp, sDACABACADp, sDACABCACDp, sDACBCACBDp,sDBABCABADp, sDBABCBABDp, sDBACABACDp, sDBCACBCADp, sDBCBABCBDp, sDBCBACBCDp,sDCABACABDp, sDCACBACADp, sDCACBCACDp, sDCBABCBADp, sDCBCABCBDp, sDCBCACBCDp'.replace(/[^ABCDsp,]/gim, ''),
                expected = expectedStr.split(',').sort();
            blockB.combineBlocks(blocksForFixedABC).map(function(v) {
                return blockB.blocksToString(v);
            }).sort().should.deep.equal(expected);
        });
    });
});