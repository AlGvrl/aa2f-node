(function(){
    'use strict';
    var cp = require('child_process'),
        path = require('path'),
        chai = require('chai'),
        should = chai.should(),
        mocha = require('mocha'),
        gen = require('../src/generator.js'),
        sinon = require('sinon');

    describe('workerProcess', function() {
        var child,
            result;

        function msgHandler(msg){
            result = msg;
        };

        beforeEach(function(){
            child = cp.fork(path.join(__dirname,'../src/workerProcess.js'));
            child.on('message', msgHandler);
        });

        afterEach(function(){
            child.kill();
            result = null;
        });

        it('should send empty array when initialized', function(done) {
            setTimeout(function(){
                result.should.deep.equal([]);
                done();
            },750);
        });

        describe('It should pass generator:generate\'s tests', function() {
            it('should return an alphabet if receives empty string', function (done) {
                child.send({parentStrings:[''], cardinality:3});
                setTimeout(function(){
                    result.should.deep.equal(['a', 'b', 'c']);
                    done();
                },750);
            });

            it('should return only AA2F children', function (done) {
                child.send({parentStrings:['abb'], cardinality:3});
                setTimeout(function(){
                    result.should.deep.equal(['abbb', 'abbc']);
                    done();
                },750);
            });
        });

        it('should be able to process multiple strings at once', function(done){
            child.send({
                parentStrings: ['ab', 'ccb', 'cbab'],
                cardinality: 3
            });
            setTimeout(function(){
                result.should.deep.equal(['aba', 'abb', 'abc', 'ccba',
                                          'ccbb', 'ccbc', 'cbabb', 'cbabc']);
                done();
            },750);
        });
    });
}());
