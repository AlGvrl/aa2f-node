(function () {
    "use strict";
    var chai = require('chai'),
        should = chai.should(),
        mocha = require('mocha'),
        gen = require('../src/generator.js');

    describe('generator', function () {
        describe('generateAA2F', function () {
            it('should return an alphabet if receives empty string', function () {
                var result = gen.generateAA2F('', 3);
                result.should.deep.equal(['a', 'b', 'c']);
            });

            /*it('should return an empty array if word is not AA2F', function () {
                var result = gen.generate('abba', 3);
                result.should.deep.equal([]);
            });*/ //would need to run full check. Easy to implement, but would need a lot of time.

            it('should return only AA2F children', function () {
                var result = gen.generateAA2F('abb', 3);
                result.should.deep.equal(['abbb', 'abbc']);
            });
        });
        
        describe('generateAA2FR', function () {
            it('should return an alphabet if receives empty string', function () {
                var result = gen.generateAA2FR('', 3);
                result.should.deep.equal(['a', 'b', 'c']);
            });
            
            it('should return only AA2F children', function () {
                var result = gen.generateAA2FR('aba', 3);
                result.should.deep.equal(['abaaa', 'abac']);
            });
            
            it('should return only one child if word ends with double', function () {
                var result = gen.generateAA2FR('abb', 3);
                result.should.deep.equal(['abbb']);
            });
            
            it('should replace ...xx children by ...xxx', function() {
                var result = gen.generateAA2FR('ab', 3);
                result.should.deep.equal(['aba', 'abbb', 'abc']);
            });
            
            it('should be able to continue word ending with triple', function () {
                var result = gen.generateAA2FR('caaa', 3);
                result.should.deep.equal(['caaab', 'caaac']);
            });
        });

        describe('isAA2FPartialCheck', function () {
            it('should notice if word has repeating factors at the end', function () {
                gen.isAA2FPartialCheck('aabbaab').should.equal(false);
            });

            it('shouldn\'t notice if word has repeating factors anywhere else', function () {
                gen.isAA2FPartialCheck('baabacb').should.equal(true);
                gen.isAA2FPartialCheck('bbaabac').should.equal(true);
            });

            it('should return true for an empty word', function () {
                gen.isAA2FPartialCheck('').should.equal(true);
            });
        });

        describe('isA2F', function () {
            it('should return true for a2f word', function () {
                gen.isA2F('abacaba').should.equal(true);
            });

            it('should return false for aa2f word', function () {
                gen.isA2F('aabaac').should.equal(false);
            });
        });

        describe('isAA2FFullCheck', function () {
            it('should notice if word has repeating factors anywhere', function () {
                gen.isAA2FFullCheck('baabacb').should.equal(false);
                gen.isAA2FFullCheck('bbaabaa').should.equal(false);
                gen.isAA2FFullCheck('aabbaab').should.equal(false);
                gen.isAA2FFullCheck('abbcacb').should.equal(true);
            });
            
            it('should return true for an empty word', function () {
                gen.isAA2FFullCheck('').should.equal(true);
            });
        });

        describe('calculateParikhVector', function () {
            it('should guess cardinality if it is not specified', function () {
                gen.calculateParikhVector('aaabbc')().length.should.equal(3);
                gen.calculateParikhVector('ddddaaabbc')().length.should.equal(4);
                gen.calculateParikhVector('ddddaaa')().length.should.equal(4);
            });

            it('should return empty array for an empty word', function () {
                gen.calculateParikhVector('')().length.should.equal(0);
            });

            it("should return amount of 'a'-s at [0] and 'z'-s at [25]", function () {
                var result = gen.calculateParikhVector('aaafgjlkdfjgzzzzz')();
                result[0].should.equal(3);
                result[25].should.equal(5);
            });

            it('should only count lowercase latin letters', function () {
                var result = gen.calculateParikhVector('aFDSNKV123../^\nbbccc', null, true)();
                result.should.deep.equal([1, 2, 3]);
            });
        });
    });
}());
